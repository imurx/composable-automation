package me.sargunvohra.mcmods.composableautomation.common

import com.mojang.blaze3d.platform.GlStateManager
import net.minecraft.client.gui.screen.ingame.AbstractContainerScreen
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.network.chat.Component

class ContainerScreen1x1(
    container: GenericContainer1x1,
    playerInv: PlayerInventory,
    name: Component
) : AbstractContainerScreen<GenericContainer1x1>(container, playerInv, name) {

    override fun render(int_1: Int, int_2: Int, float_1: Float) {
        this.renderBackground()
        super.render(int_1, int_2, float_1)
        this.drawMouseoverTooltip(int_1, int_2)
    }

    override fun drawForeground(int_1: Int, int_2: Int) {
        val name = this.title.formattedText
        this.font.draw(
            name,
            (this.containerWidth / 2 - this.font.getStringWidth(name) / 2).toFloat(),
            6.0f,
            4210752
        )
        this.font.draw(
            this.playerInventory.displayName.formattedText,
            8.0f,
            (this.containerHeight - 96 + 2).toFloat(),
            4210752
        )
    }

    override fun drawBackground(float_1: Float, int_1: Int, int_2: Int) {
        GlStateManager.color4f(1.0f, 1.0f, 1.0f, 1.0f)
        this.minecraft!!.textureManager.bindTexture(TEXTURE)
        val int_3 = (this.width - this.containerWidth) / 2
        val int_4 = (this.height - this.containerHeight) / 2
        this.blit(int_3, int_4, 0, 0, this.containerWidth, this.containerHeight)
    }

    companion object {
        private val TEXTURE = id("textures/gui/container/1x1.png")
    }
}
