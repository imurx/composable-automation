package me.sargunvohra.mcmods.composableautomation.common

import net.minecraft.entity.ItemEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.inventory.Inventory
import net.minecraft.item.ItemStack

/**
 * Wraps an [ItemEntity] to make it work like an [Inventory].
 */
class ItemEntityInventory(
    val itemEntity: ItemEntity
) : Inventory {

    override fun getInvStack(slot: Int): ItemStack {
        return if (slot == 0) itemEntity.stack else ItemStack.EMPTY
    }

    override fun markDirty() {}

    override fun clear() {
        itemEntity.stack = ItemStack.EMPTY
    }

    override fun setInvStack(slot: Int, stack: ItemStack) {
        require(slot == 0)
        itemEntity.stack = stack
    }

    override fun removeInvStack(slot: Int): ItemStack {
        if (slot != 0)
            return ItemStack.EMPTY

        val ret = itemEntity.stack
        itemEntity.stack = ItemStack.EMPTY
        return ret
    }

    override fun canPlayerUseInv(player: PlayerEntity): Boolean {
        return false
    }

    override fun getInvSize(): Int {
        return 1
    }

    override fun takeInvStack(slot: Int, amount: Int): ItemStack {
        return if (slot != 0 || amount <= 0 || itemEntity.stack.isEmpty)
            ItemStack.EMPTY
        else
            itemEntity.stack.split(amount)
    }

    override fun isInvEmpty(): Boolean {
        return itemEntity.stack.isEmpty
    }
}
