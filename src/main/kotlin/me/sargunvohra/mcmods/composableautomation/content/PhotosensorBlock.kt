package me.sargunvohra.mcmods.composableautomation.content

import me.sargunvohra.mcmods.composableautomation.common.id
import net.fabricmc.fabric.api.block.FabricBlockSettings
import net.fabricmc.fabric.api.tag.TagRegistry
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.FacingBlock
import net.minecraft.block.Material
import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemPlacementContext
import net.minecraft.state.StateFactory
import net.minecraft.state.property.EnumProperty
import net.minecraft.tag.Tag
import net.minecraft.util.BlockMirror
import net.minecraft.util.BlockRotation
import net.minecraft.util.StringIdentifiable
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.world.BlockView
import net.minecraft.world.IWorld
import net.minecraft.world.World
import java.util.*

@Suppress("DEPRECATION")
sealed class PhotosensorBlock(
    private val colorTag: Tag<Block>,
    private val strongColorTag: Tag<Block>
) : FacingBlock(
    FabricBlockSettings.of(Material.STONE)
        .strength(3f, 3f)
        .build()
) {

    init {
        defaultState = stateFactory.defaultState
            .with(Props.facing, Direction.SOUTH)
            .with(Props.powered, PhotosensorPowerMode.NOT_POWERED)
    }

    override fun appendProperties(stateFactoryBuilder: StateFactory.Builder<Block, BlockState>) {
        stateFactoryBuilder.add(
            Props.facing,
            Props.powered
        )
    }

    override fun rotate(state: BlockState, rotation: BlockRotation): BlockState {
        return state.with(Props.facing, rotation.rotate(state[Props.facing]))
    }

    override fun mirror(state: BlockState, mirror: BlockMirror): BlockState {
        return state.rotate(mirror.getRotation(state[Props.facing]))
    }

    override fun getPlacementState(context: ItemPlacementContext): BlockState? {
        return this.defaultState.with(Props.facing, context.playerFacing)
    }

    override fun getStateForNeighborUpdate(
        state: BlockState,
        direction: Direction,
        otherState: BlockState,
        world: IWorld,
        pos: BlockPos,
        otherPos: BlockPos
    ): BlockState {
        if (!world.isClient
            && direction == state[Props.facing]
            && !world.blockTickScheduler.isScheduled(pos, this)
        ) {
            world.blockTickScheduler.schedule(pos, this, 2)
        }

        return super.getStateForNeighborUpdate(state, direction, otherState, world, pos, otherPos)
    }

    private fun checkTargetAndUpdate(state: BlockState, world: World, pos: BlockPos) {
        val facing = state[Props.facing]
        val targetBlock = world.getBlockState(pos.offset(facing)).block

        val colorMatches = colorTag.contains(targetBlock)
        val colorStronglyMatches = strongColorTag.contains(targetBlock)

        val prevPowered = state[Props.powered]
        val nowPowered = when {
            colorMatches && colorStronglyMatches -> PhotosensorPowerMode.STRONGLY_POWERED
            colorMatches -> PhotosensorPowerMode.POWERED
            else -> PhotosensorPowerMode.NOT_POWERED
        }

        if (prevPowered != nowPowered) {
            updatePowerState(state, world, pos, nowPowered)
        }
    }

    private fun updatePowerState(state: BlockState, world: World, pos: BlockPos, nowPowered: PhotosensorPowerMode) {
        val newState = state.with(Props.powered, nowPowered)
        world.setBlockState(pos, newState)
        updateNeighbors(newState, world, pos)
    }

    private fun updateNeighbors(state: BlockState, world: World, pos: BlockPos) {
        val behindPos = pos.offset(state[Props.facing].opposite)
        world.updateNeighbor(behindPos, this, pos)
        world.updateNeighborsExcept(behindPos, this, state[Props.facing])
    }

    override fun onScheduledTick(state: BlockState, world: World, pos: BlockPos, random: Random) {
        if (!world.isClient) {
            checkTargetAndUpdate(state, world, pos)
        }
    }

    override fun emitsRedstonePower(state: BlockState) = true

    override fun getStrongRedstonePower(state: BlockState, world: BlockView, pos: BlockPos, direction: Direction): Int {
        return state.getWeakRedstonePower(world, pos, direction)
    }

    override fun getWeakRedstonePower(state: BlockState, world: BlockView, pos: BlockPos, direction: Direction): Int {
        return if (state[Props.facing] == direction) state[Props.powered].strength else 0
    }

    override fun onBlockAdded(state: BlockState, world: World, pos: BlockPos, oldState: BlockState, boolean_1: Boolean) {
        if (state.block !== oldState.block
            && !world.isClient
            && state[Props.powered].strength > 0
        ) {
            checkTargetAndUpdate(state, world, pos)
        }
    }

    override fun onBlockRemoved(state: BlockState, world: World, pos: BlockPos, newState: BlockState, boolean_1: Boolean) {
        if (state.block !== newState.block
            && !world.isClient
            && state[Props.powered].strength > 0
        ) {
            updateNeighbors(state, world, pos)
        }
    }

    object Props {
        val facing = FACING!!
        val powered = PhotosensorPowerMode.prop
    }

    object Red : PhotosensorBlock(PhotosensorTags.red, PhotosensorTags.strongRed)
    object Green : PhotosensorBlock(PhotosensorTags.green, PhotosensorTags.strongGreen)
    object Blue : PhotosensorBlock(PhotosensorTags.blue, PhotosensorTags.strongBlue)
}

enum class PhotosensorPowerMode(val strength: Int) : StringIdentifiable {
    NOT_POWERED(0),
    POWERED(8),
    STRONGLY_POWERED(15);

    override fun asString() = toString().toLowerCase()

    companion object {
        val prop = EnumProperty.create("powered", PhotosensorPowerMode::class.java)!!
    }
}

object PhotosensorTags {
    val red = TagRegistry.block(id("red"))!!
    val green = TagRegistry.block(id("green"))!!
    val blue = TagRegistry.block(id("blue"))!!

    val strongRed = TagRegistry.block(id("strong_red"))!!
    val strongGreen = TagRegistry.block(id("strong_green"))!!
    val strongBlue = TagRegistry.block(id("strong_blue"))!!
}

sealed class PhotosensorItem(block: PhotosensorBlock)
    : BlockItem(block, Item.Settings().itemGroup(ItemGroup.REDSTONE)) {
    object Red : PhotosensorItem(PhotosensorBlock.Red)
    object Green : PhotosensorItem(PhotosensorBlock.Green)
    object Blue : PhotosensorItem(PhotosensorBlock.Blue)
}
