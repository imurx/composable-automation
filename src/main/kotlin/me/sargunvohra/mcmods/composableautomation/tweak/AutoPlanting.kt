@file:JvmName("AutoPlanting")

package me.sargunvohra.mcmods.composableautomation.tweak

import me.sargunvohra.mcmods.composableautomation.config.ConfigModule
import net.minecraft.block.PlantBlock
import net.minecraft.entity.ItemEntity
import net.minecraft.item.BlockItem
import net.minecraft.util.math.BlockPos

fun attemptAutoPlant(entity: ItemEntity) {
    if (ConfigModule.config.tweaks.enableAutoPlanting) {
        val world = entity.entityWorld
        if (world.isClient) return

        val stack = entity.stack
        if (stack.isEmpty) return

        val item = stack.item as? BlockItem ?: return

        val block = item.block as? PlantBlock ?: return

        val blockPos = BlockPos(entity.pos.add(0.0, 0.1, 0.0))
        if (!world.getBlockState(blockPos).isAir) return

        val defaultState = block.defaultState
        if (defaultState.canPlaceAt(world, blockPos)) {
            world.setBlockState(blockPos, defaultState)
            val newStack = stack.copy()
            newStack.addAmount(-1)
            entity.stack = newStack
        }
    }
}
