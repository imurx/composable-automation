package me.sargunvohra.mcmods.composableautomation.tweak.recipe

import com.google.gson.JsonSyntaxException
import me.sargunvohra.mcmods.composableautomation.mixin.AccessFluidTags
import me.sargunvohra.mcmods.composableautomation.tweak.TweakModule
import net.minecraft.fluid.Fluid
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.recipe.Ingredient
import net.minecraft.tag.Tag
import net.minecraft.util.Identifier
import net.minecraft.util.JsonHelper

class SoakingRecipe(
    id: Identifier,
    input: Ingredient,
    output: ItemStack,
    bonus: Identifier?,
    val fluidTag: Identifier
) : ItemEntityRecipe(id, input, output, bonus) {

    override fun getType() = Type

    override fun getSerializer() = Serializer

    override fun getRecipeKindIcon() = ItemStack(Items.WATER_BUCKET)

    object Type : ItemEntityRecipe.Type<SoakingRecipe>(TweakModule.soaking)

    object Serializer : ItemEntityRecipe.Serializer<SoakingRecipe>(
        createFromJson = { id, input, output, bonus ->
            val tagStr = JsonHelper.getString(this, "fluidtag")
            val fluidTag = Identifier.ofNullable(tagStr)
            AccessFluidTags.getContainer()[fluidTag]
                ?: throw JsonSyntaxException("Unknown fluid tag $tagStr")
            SoakingRecipe(id, input, output, bonus, fluidTag!!)
        },
        createFromPacket = { id, input, output, bonus ->
            val fluidTag = readIdentifier()
            SoakingRecipe(id, input, output, bonus, fluidTag)
        },
        writeExtraDataToPacket = { writeIdentifier(it.fluidTag) }
    )
}
