package me.sargunvohra.mcmods.composableautomation.tweak

import me.sargunvohra.mcmods.composableautomation.InitModule
import me.sargunvohra.mcmods.composableautomation.common.id
import me.sargunvohra.mcmods.composableautomation.config.ConfigModule
import me.sargunvohra.mcmods.composableautomation.tweak.recipe.BurningRecipe
import me.sargunvohra.mcmods.composableautomation.tweak.recipe.ExplodingRecipe
import me.sargunvohra.mcmods.composableautomation.tweak.recipe.SoakingRecipe
import net.fabricmc.fabric.api.tag.TagRegistry
import net.minecraft.block.DispenserBlock
import net.minecraft.item.Items
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry

object TweakModule : InitModule {

    val burning = id("burning")
    val exploding = id("exploding")
    val soaking = id("soaking")

    val toggleable = TagRegistry.block(id("toggleable"))

    override fun initCommon() {
        if (ConfigModule.config.recipes.enableExploding) {
            Registry.register(Registry.RECIPE_TYPE, exploding, ExplodingRecipe.Type)
            Registry.register(Registry.RECIPE_SERIALIZER, exploding, ExplodingRecipe.Serializer)
        }

        if (ConfigModule.config.recipes.enableBurning) {
            Registry.register(Registry.RECIPE_TYPE, burning, BurningRecipe.Type)
            Registry.register(Registry.RECIPE_SERIALIZER, burning, BurningRecipe.Serializer)
        }

        if (ConfigModule.config.recipes.enableSoaking) {
            Registry.register(Registry.RECIPE_TYPE, soaking, SoakingRecipe.Type)
            Registry.register(Registry.RECIPE_SERIALIZER, soaking, SoakingRecipe.Serializer)
        }

        if (ConfigModule.config.tweaks.enableDispenserFeeding) {
            ConfigModule.config.tweaks.dispenserFeedingWhitelist
                .map { id -> Registry.ITEM.get(Identifier.ofNullable(id)) }
                .forEach { DispenserBlock.registerBehavior(it, DispenserFeeding) }
        }

        if (ConfigModule.config.tweaks.enableDispenserToggling) {
            DispenserBlock.registerBehavior(Items.STICK, DispenserToggling)
        }
    }
}
