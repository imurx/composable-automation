package me.sargunvohra.mcmods.composableautomation

import me.sargunvohra.mcmods.composableautomation.common.CommonModule
import me.sargunvohra.mcmods.composableautomation.config.ConfigModule
import me.sargunvohra.mcmods.composableautomation.content.ContentModule
import me.sargunvohra.mcmods.composableautomation.tweak.TweakModule

interface InitModule {
    fun initCommon() {}
    fun initClient() {}
}

private val modules = setOf(
    ConfigModule,
    CommonModule,
    TweakModule,
    ContentModule
)

@Suppress("unused")
fun init() = modules.forEach { it.initCommon() }

@Suppress("unused")
fun initClient() = modules.forEach { it.initClient() }
