package me.sargunvohra.mcmods.composableautomation.config

import me.sargunvohra.mcmods.autoconfig1.ConfigData
import me.sargunvohra.mcmods.autoconfig1.annotation.Config
import me.sargunvohra.mcmods.autoconfig1.annotation.ConfigEntry

@Config(name = "composableautomation")
@Config.Gui.Background("textures/block/stone_bricks.png")
data class ModConfig(

    @ConfigEntry.Category("recipes")
    @ConfigEntry.Gui.TransitiveObject
    val recipes: RecipeFields = RecipeFields(),

    @ConfigEntry.Category("tweaks")
    @ConfigEntry.Gui.TransitiveObject
    val tweaks: TweaksFields = TweaksFields()

) : ConfigData {

    data class RecipeFields(
        val enableExploding: Boolean = true,
        val enableBurning: Boolean = true,
        val enableSoaking: Boolean = true
    )

    data class TweaksFields(
        val enableAutoPlanting: Boolean = true,
        val enableDispenserFeeding: Boolean = true,
        val enableDispenserToggling: Boolean = true,
        val dispenserFeedingWhitelist: List<String> = listOf(
            "minecraft:golden_apple",
            "minecraft:golden_carrot",
            "minecraft:wheat",
            "minecraft:carrot",
            "minecraft:potato",
            "minecraft:beetroot",
            "minecraft:wheat_seeds",
            "minecraft:pumpkin_seeds",
            "minecraft:melon_seeds",
            "minecraft:beetroot_seeds",
            "minecraft:porkchop",
            "minecraft:beef",
            "minecraft:chicken",
            "minecraft:rabbit",
            "minecraft:mutton",
            "minecraft:rotten_flesh",
            "minecraft:cod",
            "minecraft:salmon",
            "minecraft:clownfish",
            "minecraft:pufferfish",
            "minecraft:dandelion",
            "minecraft:hay_block",
            "minecraft:seagrass",
            "minecraft:bamboo",
            "minecraft:sweet_berries"
        )
    )
}
