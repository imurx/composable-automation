package me.sargunvohra.mcmods.composableautomation.mixin;

import me.sargunvohra.mcmods.composableautomation.mixinapi.MixedInProps;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.HopperBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.state.StateFactory;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(HopperBlock.class)
public abstract class MixinHopperBlock extends BlockWithEntity {
    protected MixinHopperBlock(Settings settings) {
        super(settings);
        throw new IllegalStateException();
    }

    @Inject(method = "<init>", at = @At("RETURN"))
    private void setGrateDefaultState(CallbackInfo ci) {
        setDefaultState(getDefaultState().with(MixedInProps.HAS_GRATE, false));
    }

    @Inject(method = "appendProperties", at = @At("RETURN"))
    private void appendGrateProp(StateFactory.Builder<Block, BlockState> builder, CallbackInfo ci) {
        builder.add(MixedInProps.HAS_GRATE);
    }

    @Inject(method = "activate", at = @At("HEAD"), cancellable = true)
    private void createGratedHopperOnActivateWithIronBars(
            BlockState state,
            World world,
            BlockPos pos,
            PlayerEntity player,
            Hand hand,
            BlockHitResult hitResult,
            CallbackInfoReturnable<Boolean> cir
    ) {
        if (!state.get(MixedInProps.HAS_GRATE)) {
            ItemStack stack = player.getStackInHand(hand);
            if (stack.getItem() == Items.IRON_BARS && stack.getAmount() >= 1) {
                if (!world.isClient) {
                    if (!player.isCreative()) {
                        stack.addAmount(-1);
                    }
                    world.setBlockState(pos, state.with(MixedInProps.HAS_GRATE, true), 3);
                }
                cir.setReturnValue(true);
            }
        }
    }

    @Inject(
            method = "onBlockRemoved",
            at = @At(
                    value = "INVOKE",
                    target = "Lnet/minecraft/util/ItemScatterer;spawn(Lnet/minecraft/world/World;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/inventory/Inventory;)V"
            )
    )
    private void dropIronBarsOnRemoval(
            BlockState state,
            World world,
            BlockPos pos,
            BlockState newState,
            boolean unknown,
            CallbackInfo ci
    ) {
        if (state.get(MixedInProps.HAS_GRATE)) {
            ItemScatterer.spawn(world, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(Items.IRON_BARS));
        }
    }
}
